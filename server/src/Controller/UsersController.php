<?php namespace CodeIdeas\Controller;

use CodeIdeas\Core\Application;
use CodeIdeas\Entity\User;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;


class UsersController extends Controller
{
    public function index(Request $request, Application $app)
    {
        $orderClause = 'u.registeredAt DESC';
        if ('rating' === $request->get('sort', 'rating')) {
            $orderClause = 'u.rating DESC, ' . $orderClause;
        }

        $limit = $this->getLimit($request);
        $query = $app['orm.em']->createQuery(
            "SELECT u FROM CodeIdeas\\Entity\\User u ORDER BY $orderClause"
        )
            ->setFirstResult($limit * $this->getPage($request))
            ->setMaxResults($limit);

        $users = [];
        foreach (new Paginator($query) as $user) {
            $users[] = $user;
        }

        return $app->json(
            array_map(
                function(User $user) {
                    return $this->makeUserResponse($user);
                },
                $users
            )
        );
    }

    public function show(Request $request, Application $app)
    {
        $user = $app['orm.em']->find('CodeIdeas\Entity\User', $request->get('id'));
        if (empty($user)) {
            return $app->json(null, 404);
        }

        return $app->json($this->makeUserResponse($user));
    }
} 