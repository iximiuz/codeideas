<?php namespace CodeIdeas\Controller\Provider;


use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class ResourceControllerProvider implements ControllerProviderInterface
{
    const METHOD_INDEX  = 'index';  // show all
    const METHOD_SHOW   = 'show';    // show one
    const METHOD_CREATE = 'create';
    const METHOD_UPDATE = 'update';
    const METHOD_DELETE = 'delete';


    protected static $methodsMap = [
        self::METHOD_INDEX  => ['method' => 'get', 'url' => '/'],
        self::METHOD_SHOW   => ['method' => 'get', 'url' => '/{id}', 'assert' => ['id' => '\d+']],
        self::METHOD_CREATE => ['method' => 'post', 'url' => '/'],
        self::METHOD_UPDATE => ['method' => 'put', 'url' => '/{id}', 'assert' => ['id' => '\d+']],
        self::METHOD_DELETE => ['method' => 'delete', 'url' => '/{id}', 'assert' => ['id' => '\d+']],
    ];


    private $controllerName;
    private $authCallback;
    private $assertions;
    private $methods;


    public function __construct(
        $controllerName,
        $authCallback = null,
        array $assertions = [],
        array $methods = [
            self::METHOD_INDEX,
            self::METHOD_SHOW,
            self::METHOD_CREATE,
            self::METHOD_UPDATE,
            self::METHOD_DELETE,
        ]
    ) {
        $this->controllerName = $controllerName;
        $this->authCallback = $authCallback;
        $this->assertions = $assertions;
        $this->methods = $methods;
    }

    public function connect(Application $app)
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory'];

        foreach ($this->methods as $controllerMethod) {
            if (
                !empty(self::$methodsMap[$controllerMethod]['method'])
                && !empty(self::$methodsMap[$controllerMethod]['url'])
            ) {
                $meta = self::$methodsMap[$controllerMethod];
                $request = $controllers->{$meta['method']}($meta['url'], $this->controllerName . '::' . $controllerMethod);

                if (!empty($meta['assert'])) {
                    foreach ($meta['assert'] as $key => $value) {
                        $request->assert($key, $value);
                    }
                }
            } else {
                $app['logger']->warning("Skip incompatible method [$controllerMethod]");
            }
        }

        foreach ($this->assertions as $key => $value) {
            $controllers->assert($key, $value);
        }

        if (is_callable($this->authCallback)) {
            $controllers->before($this->authCallback, Application::EARLY_EVENT);
        }

        return $controllers;
    }
}