<?php namespace CodeIdeas\Controller;

use CodeIdeas\Core\Application;
use CodeIdeas\Entity\Idea;
use CodeIdeas\Entity\Session;
use CodeIdeas\Entity\SocialProfile;
use CodeIdeas\Entity\User;
use CodeIdeas\Service\Rating\RatingService;
use Symfony\Component\HttpFoundation\Request;


class AuthController extends Controller
{
    public function create(Request $request, Application $app)
    {
        if ($request->get('externalToken')) {
            return $this->enterBySocialToken($request, $app);
        }

        if ($request->get('email') && $request->get('password')) {
            return $this->enterByEmailAndPassword($request, $app);
        }

        $app['logger']->error('Missed required params.');

        $app->abortJson(['error' => true, 'msg' => 'Missed required params.'], 400);
    }

    public function delete(Request $request, Application $app)
    {
        // todo: ...
    }

    public function index(Request $request, Application $app)
    {
        $token = $request->get('token');
        if (empty($token)) {
            $app->abortJson(['error' => true, 'msg' => 'Missed required param [token].']);
        }

        /** @var Session $session */
        $session = $app['orm.em']->find('CodeIdeas\Entity\Session', $token);
        if (empty($session)) {
            $app->abortJson(['status' => 'Bad or expired token.']);
        }

        $user = $session->getUser();
        if (empty($user)) {
            $app['logger']->error(
                'User [' . $session->getUserId() . '] not found for session [' . $session->getId(). ']'
            );
            $app->abortJson(['error' => true, 'msg' => 'User not found.']);
        }

        return $app->json($this->makeResponse('loggedIn', $user, $session));
    }

    /*******************************************************************************************************************/

    private function ensureSocialUserDataCorrectness(array $data, Application $app)
    {
        if (empty($data['network']) || empty($data['uid'])) {
            $app['logger']->error(
                'Social auth failure. Unexpected format. Response [' . var_export($data, 1) . ']'
            );

            $app->abortJson(['error' => true, 'msg' => 'Social auth failure.']);
        }
    }

    private function enterByEmailAndPassword(Request $request, Application $app)
    {
        $email = $request->get('email');
        $password = trim((string)$request->get('password'));

        $status = 'loggedIn';

        // пытаемся найти пользователя по email (связать соц. профили)
        /** @var User $user */
        $user = $app['orm.em']->getRepository('CodeIdeas\Entity\User')->findOneBy(['email' => $email]);
        if (empty($user)) {
            // создаем пользователя, если его еще не существовало или текущая социалка не сообщает email
            $user = new User(substr($email, 0, strpos($email, '@')), '', $email);
            $user->setPasswordHash($password);

            $app['rating']->ratingAward($user, RatingService::RATING_REGISTRATION);

            $app['orm.em']->persist($user);
            $app['orm.em']->flush();

            $status = 'signedIn';
        } elseif ($user->getPasswordHash() !== User::hashPassword($password)) {
            $app['logger']->debug('wrong password hash [' . User::hashPassword($password) . ']');
            $app->abortJson(['error' => true, 'msg' => 'No such user or password'], 409);
        }

        return $this->handleEnterBase($user, $status, $request, $app);
    }

    private function enterBySocialToken(Request $request, Application $app)
    {
        $url = 'http://ulogin.ru/token.php?token=' . $request->get('externalToken') . '&host=' . $request->get('targetHost');
        $s = file_get_contents($url);

        if (empty($s)) {
            $app['logger']->error('Social auth failure. Url [' . $url . ']');
            $app->abortJson(['error' => true, 'msg' => 'Social auth failure.']);
        }

        $socialNetworkUser = json_decode($s, true);
        if (empty($socialNetworkUser) || !empty($socialNetworkUser['error'])) {
            $app['logger']->error(
                'Social auth failure. Url [' . $url . ']. Response [' . var_export($socialNetworkUser, 1) . ']'
            );
            $app->abortJson(['error' => true, 'msg' => 'Social auth failure.']);
        }

        $this->ensureSocialUserDataCorrectness($socialNetworkUser, $app);

        /** @var SocialProfile $socialProfile */
        $socialProfile = $app['orm.em']->getRepository('CodeIdeas\Entity\SocialProfile')->findOneBy(
            [
                'network' => $socialNetworkUser['network'],
                'uid'     => $socialNetworkUser['uid']
            ]
        );

        $status = 'loggedIn';
        if (empty($socialProfile)) {
            if (!empty($socialNetworkUser['email'])) {
                // пытаемся найти пользователя по email (связать соц. профили)
                $user = $app['orm.em']->getRepository('CodeIdeas\Entity\User')->findOneBy(
                    ['email' => $socialNetworkUser['email']]
                );
            }

            if (empty($user)) {
                // создаем пользователя, если его еще не существовало или текущая социалка не сообщает email
                $user = new User(
                    $socialNetworkUser['first_name'],
                    @$socialNetworkUser['last_name'] ?: '',
                    @$socialNetworkUser['email'] ?: null
                );

                if (!empty($socialNetworkUser['photo'])) {
                    $user->setAvatar($socialNetworkUser['photo']); // 100x100
                } elseif (!empty($socialNetworkUser['photo_big'])) {
                    $user->setAvatar($socialNetworkUser['photo_big']); // quadratize
                }

                $app['rating']->ratingAward($user, RatingService::RATING_REGISTRATION);

                $app['orm.em']->persist($user);
                $status = 'signedIn';
            }

            // создать соц. профиль
            $socialProfile = SocialProfile::create($socialNetworkUser);
            $socialProfile->addLinkedUser($user);

            $app['orm.em']->persist($socialProfile);

            $app['orm.em']->flush();
        } else {
            // достать пользователя
            // todo: обновить данные $socialProfile, если они отличаются от $socialNetworkUser
            $user = $socialProfile->getLinkedUser();
        }

        return $this->handleEnterBase($user, $status, $request, $app);
    }

    public function handleEnterBase(User $user, $status, Request $request, Application $app)
    {
        // создаем сессию
        $session = new Session($request->get('externalToken'), $user);
        $app['orm.em']->persist($session);

        if ($token = $request->get('token')) {
            // переносим идеи из временного пользователя
            /** @var Session $tmpSession */
            $tmpSession = $app['orm.em']->find('CodeIdeas\Entity\Session', $token);
            if (!empty($tmpSession) && !empty($tmpSession->getUser())) {
                /** @var Idea[] $ideas */
                $ideas = $app['orm.em']->createQuery(
                    "SELECT i, u FROM CodeIdeas\\Entity\\Idea i LEFT JOIN i.user u WHERE u.id = {$tmpSession->getUser()->getId()}"
                )->getResult();

                foreach ($ideas as $idea) {
                    $idea->setUser($user);
                    $app['orm.em']->persist($idea);
                }
            }
        }

        $app['orm.em']->flush();

        return $app->json($this->makeResponse($status, $user, $session));
    }

    private function makeResponse($status, User $user, Session $session)
    {
        return [
            'status' => $status,
            'user' => $this->makeUserResponse($user) + ['token' => $session->getId()]
        ];
    }
} 