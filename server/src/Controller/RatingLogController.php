<?php namespace CodeIdeas\Controller;

use CodeIdeas\Core\Application;
use CodeIdeas\Entity\UserRatingLog;
use Symfony\Component\HttpFoundation\Request;


class RatingLogController extends Controller
{
    public function index(Request $request, Application $app)
    {
        $logEntries = $app['orm.em']->getRepository('CodeIdeas\Entity\UserRatingLog')->findBy(
            ['user' => $request->get('userId')],
            ['createdAt' => 'DESC'],
            $this->getLimit($request, 'limit', 5)
        );

        return $app->json(
            array_map(
                function(UserRatingLog $log) { return $this->makeRatingLogResponse($log); },
                $logEntries
            )
        );
    }

    /******************************************************************************************************************/

    private function makeRatingLogResponse(UserRatingLog $log)
    {
        return [
            'amount' => $log->getAmount(),
            'event' => $log->getEventType(),
            'createdAt' => $log->getCreatedAt(),
        ];
    }
} 