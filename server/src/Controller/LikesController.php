<?php namespace CodeIdeas\Controller;

use CodeIdeas\Core\Application;
use CodeIdeas\Entity\Idea;
use CodeIdeas\Service\Rating\RatingService;
use CodeIdeas\Service\Voting\VotingService;
use Symfony\Component\HttpFoundation\Request;


class LikesController extends Controller
{
    const TAG_IMPL = 'impl';
    const TAG_LIKE = 'like';


    public function create(Request $request, Application $app)
    {
        $idea = $this->getIdea($request, $app);

        switch ($request->get('tag')) {
            case self::TAG_LIKE:
                $this->processIdeaLike($idea, $app);
                break;

            case self::TAG_IMPL:
                $this->processIdeaWantToImpl($idea, $app);
                break;

            default:
                $app->abortJson(['error' => true, 'msg' => 'Unknown tag.']);
                break;
        }

        $app['orm.em']->persist($idea);
        $app['orm.em']->flush();

        $this->loadVotingDataIfNeeded([$idea], $app); // todo: избавиться от потенциально лишнего запроса

        return $app->json($this->makeIdeaResponse($idea, true));
    }

    public function delete(Request $request, Application $app)
    {
        $idea = $this->getIdea($request, $app);

        switch ($request->get('tag')) {
            case self::TAG_LIKE:
                $this->processIdeaNoLongerLike($idea, $app);
                break;

            case self::TAG_IMPL:
                $this->processIdeaNoLongerWantToImpl($idea, $app);
                break;

            default:
                $app->abortJson(['error' => true, 'msg' => 'Unknown tag.']);
                break;
        }

        $app['orm.em']->persist($idea);
        $app['orm.em']->flush();

        $this->loadVotingDataIfNeeded([$idea], $app); // todo: избавиться от потенциально лишнего запроса

        return $app->json($this->makeIdeaResponse($idea, true));
    }

    /******************************************************************************************************************/

    private function getIdea(Request $request, Application $app)
    {
        /** @var Idea $idea */
        $idea = $app['orm.em']->find('CodeIdeas\Entity\Idea', $request->get('ideaId'));
        if (empty($idea)) {
            $app->abortJson(['msg' => 'No such idea.'], 400);
        }

        return $idea;
    }

    private function processIdeaLike(Idea $idea, Application $app)
    {
        if ($app['user']->getId() === $idea->getUser()->getId()) {
            $app->abortJson(['error' => true, 'msg' => 'Cannot like own idea.'], 412);
        }

        $this->vote($idea, 'like', $app);
        $idea->like();

        $app['rating']->ratingAward(
            $idea->getUser(),
            RatingService::RATING_INCR_IDEA_LIKE,
            ['ideaId' => $idea->getId()]
        );
    }

    private function processIdeaNoLongerLike(Idea $idea, Application $app)
    {
        $this->removeVote($idea, 'like', $app);
        $idea->noLongerLike();

        $app['rating']->ratingReward(
            $idea->getUser(),
            RatingService::RATING_INCR_IDEA_LIKE,
            ['ideaId' => $idea->getId()]
        );
    }

    private function processIdeaNoLongerWantToImpl(Idea $idea, Application $app)
    {
        $this->removeVote($idea, 'impl', $app);
        $idea->noLongerWantToImplement();

        $app['rating']->ratingReward(
            $idea->getUser(),
            RatingService::RATING_INCR_IDEA_WANT_TO_IMPL,
            ['ideaId' => $idea->getId()]
        );
    }

    private function processIdeaWantToImpl(Idea $idea, Application $app)
    {
        $this->vote($idea, 'impl', $app);
        $idea->wantToImplement();

        $app['rating']->ratingAward(
            $idea->getUser(),
            RatingService::RATING_INCR_IDEA_WANT_TO_IMPL,
            ['ideaId' => $idea->getId()]
        );
    }

    private function removeVote(Idea $idea, $tag, Application $app)
    {
        /** @var VotingService $votingService */
        $votingService = $app['voting'];

        $votingService->removeVote('idea', $idea->getId(), $app['user']->getId(), 1, $tag);
    }

    private function vote(Idea $idea, $tag, Application $app)
    {
        /** @var VotingService $votingService */
        $votingService = $app['voting'];

        $votingService->vote('idea', $idea->getId(), $app['user']->getId(), 1, date('Y-m-d H:i:s'), $tag);
    }
} 