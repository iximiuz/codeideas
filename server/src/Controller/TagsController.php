<?php namespace CodeIdeas\Controller;

use CodeIdeas\Core\Application;
use CodeIdeas\Entity\Tag;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\HttpFoundation\Request;


class TagsController extends Controller
{
    const MAX_TAGS_SUGGEST = 5;


    public function index(Request $request, Application $app)
    {
        if ($userId = $request->get('userId')) {
            return $this->findUserTags($userId, $request, $app);
        }

        if ($q = $request->get('q')) {
            if ('popular' === $q) {
                $tags = $app['orm.em']->getRepository('CodeIdeas\Entity\Tag')->findBy([], ['taggedCount' => 'DESC'], 10);

                return $app->json(
                    array_map(
                        function(Tag $tag) {
                            return $this->makeTagResponse($tag);
                        },
                        $tags
                    )
                );
            }

            $app->abortJson(['error' => true, 'Bad request data.'], 400);
        }


        $mask = $request->get('mask');
        $this->ensureMaskFormat($mask, $app);

        $tags = $app['orm.em']->getRepository('CodeIdeas\Entity\Tag')->createQueryBuilder('t')
            ->where('t.text LIKE :mask')
            ->setParameter('mask', "%$mask%")
            ->getQuery()
            ->setMaxResults(self::MAX_TAGS_SUGGEST)
            ->getResult();

        return $app->json(
            array_map(
                function(Tag $tag) {
                    return $this->makeTagResponse($tag);
                },
                $tags
            )
        );
    }

    /******************************************************************************************************************/

    private function ensureMaskFormat($mask, Application $app)
    {
        $mask = $mask ?: '';
        $mask = preg_replace('/\s+/', '-', $mask);

        if (empty($mask) || !Tag::isValidTag($mask)) {
            $app->abortJson(['error' => true, 'msg' => 'Not allowed data in mask.'], 400);
        }
    }

    private function findUserTags($userId, Request $request, Application $app)
    {
        if ('used' === $request->get('q')) {
            return $this->findUserUsedTags($userId, $app);
        }

        return $app->json(
            array_map(
                function(Tag $tag) {
                    return $this->makeTagResponse($tag);
                },
                $app['orm.em']->getRepository('CodeIdeas\Entity\Tag')->findBy(['owner' => $userId])
            )
        );
    }

    private function findUserUsedTags($userId, Application $app)
    {
        $rsm = new ResultSetMappingBuilder($app['orm.em']);
        $rsm->addScalarResult('tag_id', 'tId');
        $query = $app['orm.em']->createNativeQuery('SELECT DISTINCT tag_id FROM ideas_tags WHERE tagger_id = ?', $rsm);
        $query->setParameter(1, $userId);

        $tagIds = array_map(function(array $data) { return $data['tId']; }, $query->getResult());
        $query = $app['orm.em']->createQueryBuilder()->select('t')->from('CodeIdeas\Entity\Tag', 't')->where('t.id IN (:ids)')->getQuery();

        return $app->json(
            array_map(
                function(Tag $tag) {
                    return $this->makeTagResponse($tag);
                },
                $query->setParameter('ids', $tagIds)->getResult()
            )
        );
    }
} 