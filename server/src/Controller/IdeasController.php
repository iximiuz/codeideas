<?php namespace CodeIdeas\Controller;

use CodeIdeas\Core\Application;
use CodeIdeas\Entity\Idea;
use CodeIdeas\Entity\Session;
use CodeIdeas\Entity\Tag;
use CodeIdeas\Entity\User;
use CodeIdeas\Service\Rating\RatingService;
use CodeIdeas\Service\Voting\Exception\VotingServiceException;
use CodeIdeas\Service\Voting\VotingService;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;


class IdeasController extends Controller
{
    public function create(Request $request, Application $app)
    {
        $response = [];
        if (empty($app['user'])) {
            // создаем временного пользователя
            $app['user'] = new User(
                'Пользователь',
                rand()
            );

            $app['user']->markAsTmp();

            $app['orm.em']->persist($app['user']);
            $app['orm.em']->flush();

            $app['user'] = $app['orm.em']->find('CodeIdeas\Entity\User', $app['user']->getId());

            $response['user'] = $this->makeUserResponse($app['user']);

            // создаем сессию
            $session = new Session($request->get('externalToken'), $app['user']);
            $app['orm.em']->persist($session);
            $app['orm.em']->flush();

            $response['user']['token'] = $session->getId();
            $response['user']['isTmp'] = true;
        }

        $idea = new Idea(
            $app['user'],
            substr($request->get('title'), 0, Idea::MAX_TITLE_SIZE),
            substr($request->get('description'), 0, Idea::MAX_DESCRIPTION_SIZE),
            substr($request->get('profit'), 0, Idea::MAX_PROFIT_SIZE),
            $this->requestDevTimeToIdeaImplTime($request->get('devTime')),
            $request->get('teamRequired') ? Idea::TEAM_TYPE_MEDIUM : null,
            date('Y-m-d H:i:s')
        );

        $tags = $request->get('tags', []);
        if (is_array($tags) && !empty($tags)) {
            $tags = $this->addNewTagsToIdea($idea, $tags, $app);
        }


        $app['orm.em']->persist($idea);
        $app['orm.em']->flush();

        // todo: move it to asyn execution
        if (!empty($tags) && reset($tags) instanceof Tag) {
            $this->assignTagUseAward($tags, $app);
        }

        return $app->json(['id' => $idea->getId()] + $response);
    }

    public function index(Request $request, Application $app)
    {
        $orderClause = 'i.createdAt DESC';
        if ('popular' === $request->get('sort')) {
            $orderClause = 'i.rating DESC, ' . $orderClause;
        }

        $whereClause = '';
        $tags = array_filter(explode(',', $request->get('tags', '')));
        if (!empty($tags)) {
            $c = 0;
            $whereClause = 'WHERE t.text IN (' .
                    join(',', array_map(function() use (&$c) { return ':tag' . $c++; }, $tags)) .
                ')';
        }

        if (!empty($request->get('userId'))) {
            $whereClause .= ($whereClause ? ' AND' : 'WHERE') . ' u.id = :uId';
        }

        $likedBy = $request->get('likedBy');
        $implBy = $request->get('implBy');
        if ($likedBy || $implBy) {
            /** @var VotingService $votingService */
            $votingService = $app['voting'];
            try {
                $votes = $votingService->getVotes(
                    $likedBy ?: $implBy,
                    'idea',
                    [empty($likedBy) ? 'impl' : 'like'],
                    $this->getLimit($request),
                    $this->getLimit($request) * $this->getPage($request)
                );
            } catch (VotingServiceException $e) {
                $app['logger']->error((string)$e);
                $votes = [];
            }

            $ideaIds = [];
            foreach ($votes as $voteData) {
                if (!empty($voteData['voteeId'])) {
                    $ideaIds[] = $voteData['voteeId'];
                }
            }

            if (empty($ideaIds)) {
                return $app->json([]);
            }

            $whereClause = 'WHERE i.id IN (:ideaIds)';
        }

        /** @var Query $query */
        $query = $app['orm.em']->createQuery(
            "SELECT i, u, t FROM CodeIdeas\\Entity\\Idea i LEFT JOIN i.user u LEFT JOIN i.tags t $whereClause ORDER BY $orderClause"
        );

        if (!isset($ideaIds)) {
            $query->setFirstResult($this->getLimit($request) * $this->getPage($request))
                ->setMaxResults($this->getLimit($request));
        }

        foreach ($tags as $idx => $t) {
            $query->setParameter('tag' . $idx, $t);
        }

        if (!empty($request->get('userId'))) {
            $query->setParameter('uId', $request->get('userId'));
        }

        if (!empty($ideaIds)) {
            $query->setParameter('ideaIds', $ideaIds);
        }

        /** @var Idea[] $ideas */
        $ideas = [];
        foreach (new Paginator($query) as $idea) {
            $ideas[] = $idea;
        }

        $this->loadVotingDataIfNeeded($ideas, $app);

        return $app->json(
            array_map(
                function (Idea $idea) {
                    return $this->makeIdeaResponse($idea);
                },
                $ideas
            )
        );
    }

    public function show(Request $request, Application $app)
    {
        $idea = $app['orm.em']->find('CodeIdeas\Entity\Idea', $request->get('id'));
        if (empty($idea)) {
            $app->abortJson(['msg' => 'No such resource.'], 404);
        }

        $this->loadVotingDataIfNeeded([$idea], $app);

        return $app->json($this->makeIdeaResponse($idea, true));
    }

    public function update(Request $request, Application $app)
    {
        /** @var Idea $idea */
        $idea = $app['orm.em']->find('CodeIdeas\Entity\Idea', $request->get('id'));
        if (empty($idea)) {
            $app->abortJson(['msg' => 'No such resource.'], 404);
        }

        if (empty($app['user'])) {
            $app->abortJson(['error' => true, 'msg' => 'Auth required.'], 401);
        }

        if ($idea->getUser()->getId() != $app['user']->getId()) {
            $app->abortJson(['error' => true, 'msg' => 'Not allowed.'], 403);
        }

        $title = $request->get('title');
        if ($title && $idea->getTitle() !== $title) {
            $idea->setTitle(substr($title, 0, Idea::MAX_TITLE_SIZE));
        }

        $description = $request->get('description');
        if ($description && $idea->getDescription() !== $description) {
            $idea->setDescription(substr($description, 0, Idea::MAX_DESCRIPTION_SIZE));
        }

        $profit = $request->get('profit');
        if ($profit && $idea->getProfit() !== $profit) {
            $idea->setProfit(substr($profit, 0, Idea::MAX_PROFIT_SIZE));
        }

        if ($idea->getImplementationTime() !== $this->requestDevTimeToIdeaImplTime($request->get('devTime'))) {
            $idea->setImplementationTime($this->requestDevTimeToIdeaImplTime($request->get('devTime')));
        }

        $idea->setTeamType($request->get('teamRequired') ? Idea::TEAM_TYPE_MEDIUM : null);


        $modifiedTagsSet = $request->get('tags', []);
        if (is_array($modifiedTagsSet) && !empty($modifiedTagsSet)) {
            foreach ($idea->getTags() as $tag) {
                if (
                    false === array_search(
                        $tag->getText(),
                        array_map(
                            function($tagData) { return isset($tagData['text']) ? $tagData['text'] : ''; },
                            $modifiedTagsSet
                        )
                    )
                ) {
                    $idea->getTags()->removeElement($tag);
                }
            }
        }

        $newTags = [];
        foreach ($modifiedTagsSet as $tagData) {
            if (
                isset($tagData['text'])
                && !$idea->getTags()->filter(
                    function(Tag $tag) use ($tagData) {
                        return $tag->getText() === $tagData['text'];
                    }
                )->count()
            ) {
                $newTags[] = $tagData;
            }
        }

        if (!empty($newTags)) {
            $tags = $this->addNewTagsToIdea($idea, $newTags, $app);
        }

        $app['orm.em']->persist($idea);
        $app['orm.em']->flush();

        if (!empty($tags)) {
            $this->assignTagUseAward($tags, $app);
        }

        return $app->json($this->makeIdeaResponse($idea, true));
    }

    /******************************************************************************************************************/

    private function addNewTagsToIdea(Idea $idea, array $tags, Application $app)
    {
        $linkedTags = [];
        foreach ($tags as $tagData) {
            if (!empty($tagData['text']) && Tag::isValidTag(trim($tagData['text']))) {
                $linkedTags[] = [
                    'text' => '"' . strtolower(trim($tagData['text'])) . '"',
                    'user_id' => $app['user']->getId(),
                    1,
                    'NULL'
                ];
            } else {
                $app['logger']->warning('Bad tag [' . var_export($tagData) . ']');
            }
        }

        if (!empty($linkedTags)) {
            $linkedTags = array_slice($linkedTags, 0, Idea::MAX_TAGS_COUNT);

            $sql = 'INSERT INTO tags (text, user_id, tagged_cnt, created_at) VALUES %s ON DUPLICATE KEY UPDATE tagged_cnt = tagged_cnt + 1';
            $app['orm.em']->getConnection()->exec(
                sprintf(
                    $sql,
                    implode(
                        ',',
                        array_map(
                            function (array $tag) {
                                return '(' . implode(',', $tag) . ')';
                            },
                            $linkedTags
                        )
                    )
                )
            );

            /** @var Tag[] $tags */
            $tags = $app['orm.em']->getRepository('CodeIdeas\Entity\Tag')->findBy(
                [
                    'text' => array_map(
                        function(array $tag) {
                            return trim($tag['text'], '"');
                        },
                        $linkedTags
                    )
                ]
            );

            foreach ($tags as $tag) {
                $idea->getTags()->add($tag);
            }
        }

        return empty($tags) || !(reset($tags) instanceof Tag) ? [] : $tags;
    }

    private function assignTagUseAward(array $tags, Application $app)
    {
        /** @var RatingService $ratingService */
        $ratingService = $app['rating'];

        foreach ($tags as $tag) {
            if ($tag->getOwner()->getId() != $app['user']->getId()) {
                /** @var Tag $tag */
                $ratingService->ratingAward($tag->getOwner(), RatingService::RATING_INCR_TAG_USED);
                $app['orm.em']->persist($tag->getOwner());
            }
        }

        $app['orm.em']->flush();
    }

    private function requestDevTimeToIdeaImplTime($devTime)
    {
        $map = [
            'hours' => Idea::IMPL_TIME_HOURS,
            'days' => Idea::IMPL_TIME_DAYS,
            'weeks' => Idea::IMPL_TIME_WEEKS,
            'months' => Idea::IMPL_TIME_MONTHS,
            'geHalfOfYear' => Idea::IMPL_TIME_GE_HALF_OF_YEAR,
        ];

        return isset($map[$devTime]) ? $map[$devTime] : null;
    }
} 