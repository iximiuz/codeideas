<?php namespace CodeIdeas\Controller;

use CodeIdeas\Core\Application;
use CodeIdeas\Entity\Comment;
use CodeIdeas\Entity\Exception\BadCommentFormatException;
use Symfony\Component\HttpFoundation\Request;


class CommentsController extends Controller
{
    public function create(Request $request, Application $app)
    {
        $idea = $app['orm.em']->find('CodeIdeas\Entity\Idea', $request->get('ideaId'));
        if (empty($idea)) {
            $app->abortJson(['msg' => 'No such idea.'], 400);
        }

        $parentComment = null;
        $parentCommentId = $request->get('parentId');
        if (!empty($parentCommentId)) {
            $parentComment = $app['orm.em']->find('CodeIdeas\Entity\Comment', $parentCommentId);
            if (empty($parentComment)) {
                $app->abortJson(['msg' => 'No such parent comment.'], 400);
            }
        }

        try {
            $comment = new Comment($request->get('text'), $app['user'], $idea, $parentComment);
            $app['orm.em']->persist($comment);
            $app['orm.em']->flush();

            return $app->json($this->makeCommentResponse($comment));
        } catch (BadCommentFormatException $e) {
            $app->abortJson(['msg' => 'Bad comment format. May be too short?'], 400);
        }
    }

    public function index(Request $request, Application $app)
    {
        $comments = [];

        $q = $request->get('q');
        if ('latest' === $q) {
            $comments = $app['orm.em']->getRepository('CodeIdeas\Entity\Comment')->findBy([], ['createdAt' => 'DESC'], 5);
        } elseif ($userId = $request->get('userId')) {
            $comments = $app['orm.em']->getRepository('CodeIdeas\Entity\Comment')->findBy(
                ['user' => $userId], ['createdAt' => 'DESC'], $this->getLimit($request)
            );
        }

        return $app->json(array_map(function(Comment $c) { return $this->makeCommentResponse($c); }, $comments));
    }

    /******************************************************************************************************************/

    private function makeCommentResponse(Comment $comment)
    {
        return [
            'id' => $comment->getId(),
            'user' => $this->makeUserResponse($comment->getAuthor()),
            'idea' => ['id' => $comment->getIdea()->getId(), 'title' => $comment->getIdea()->getTitle()],
            'text' => $comment->getText(),
            'createdAt' => $comment->getCreatedAt()->getTimestamp()
        ];
    }
} 