<?php namespace CodeIdeas\Controller;

use CodeIdeas\Core\Application;
use CodeIdeas\Entity\Comment;
use CodeIdeas\Entity\Idea;
use CodeIdeas\Entity\Tag;
use CodeIdeas\Entity\User;
use CodeIdeas\Service\Voting\Exception\VotingServiceException;
use CodeIdeas\Service\Voting\VotingService;
use Symfony\Component\HttpFoundation\Request;


abstract class Controller
{
    protected function getLimit(Request $request, $key = 'limit', $default = 20)
    {
        return min(100, max(0, (int)$request->get($key, $default)));
    }

    protected function getPage(Request $request, $key = 'page')
    {
        return max(0, ((int)$request->get($key, 1) - 1));
    }

    protected function handleUnknownAction($action, Application $app)
    {
        return $app->json(['error' => true, 'msg' => 'Unknown resource or action [' . $action . ']']);
    }

    /**
     * @param Idea[] $ideas
     * @param Application $app
     */
    protected function loadVotingDataIfNeeded(array $ideas, Application $app)
    {
        if (isset($app['user'])) {
            try {
                /** @var VotingService $votingService */
                $votingService = $app['voting'];

                $votes = $votingService->getVotesCount(
                    'idea',
                    array_map(function (Idea $idea) {
                        return $idea->getId();
                    }, $ideas),
                    ['like', 'impl'],
                    $app['user']->getId()
                );

                if (!empty($votes['tags']['like']['counters'])) {
                    foreach ($ideas as $idea) {
                        if (!empty($votes['tags']['like']['counters'][$idea->getId()])) {
                            $realLikesCount = reset($votes['tags']['like']['counters'][$idea->getId()]);
                            if ($idea->getLikesCount() != $realLikesCount) {
                                $app['logger']->error(
                                    'Inconsistent idea data denormalization [ideaId=' . $idea->getId() .
                                    ' realLikesCount=' . $realLikesCount . ' denormLikesCount=' . $idea->getLikesCount() . ']'
                                );

                                $idea->setLikesCount($realLikesCount);
                                // todo: update data in DB
                            }
                        }
                    }
                }

                if (!empty($votes['tags']['like']['voted'])) {
                    foreach ($ideas as $idea) {
                        if (!empty($votes['tags']['like']['voted'][$idea->getId()])) {
                            $idea->markAsLiked();
                        }
                    }
                }


                if (!empty($votes['tags']['impl']['counters'])) {
                    foreach ($ideas as $idea) {
                        if (!empty($votes['tags']['impl']['counters'][$idea->getId()])) {
                            $realCount = reset($votes['tags']['impl']['counters'][$idea->getId()]);
                            if ($idea->getWantToImplementCount() != $realCount) {
                                $app['logger']->error(
                                    'Inconsistent idea data denormalization [ideaId=' . $idea->getId() .
                                    ' realImplCount=' . $realCount . ' denormImplCount=' . $idea->getWantToImplementCount() . ']'
                                );

                                $idea->setWantToImplementCount($realCount);
                                // todo: update data in DB
                            }
                        }
                    }
                }

                if (!empty($votes['tags']['impl']['voted'])) {
                    foreach ($ideas as $idea) {
                        if (!empty($votes['tags']['impl']['voted'][$idea->getId()])) {
                            $idea->markAsWantedToImplement();
                        }
                    }
                }
            } catch (VotingServiceException $e) {
                $app['logger']->error((string) $e);
            }
        }
    }

    protected function makeIdeaResponse(Idea $idea, $full = false)
    {
        $result = [
            'id' => $idea->getId(),
            'title' => $idea->getTitle(),
            'description' => $idea->getDescription(),
            'commentsCount' => $idea->getCommentsCount(),
            'likesCount' => $idea->getLikesCount(),
            'wantToImplementCount' => $idea->getWantToImplementCount(),
            'createdAt' => $idea->getCreatedAt()->getTimestamp()
        ];

        if ($idea->getImplementationTime()) {
            $result['implementationTime'] = $idea->getImplementationTime();
        }

        if ($idea->getTeamType()) {
            $result['teamType'] = $idea->getTeamType();
        }

        $user = $idea->getUser();
        if (!empty($user)) {
            $result['user'] = $this->makeUserResponse($user);
        }

        foreach ($idea->getTags() as $tag) {
            $result['tags'][] = $this->makeTagResponse($tag);
        };

        if ($idea->isLiked()) {
            $result['isLiked'] = true;
        }

        if ($idea->isWantedToImplement()) {
            $result['isWantedToImpl'] = true;
        }

        if ($full) {
            $result['profit'] = $idea->getProfit();

            foreach ($idea->getComments() as $comment) {
                /** @var Comment $comment */
                $result['comments'][] = [
                    'id' => $comment->getId(),
                    'text' => $comment->getText(),
                    'user' => $this->makeUserResponse($comment->getAuthor()),
                    'parentId' => $comment->getParentComment() ? $comment->getParentComment()->getId() : null,
                    'createdAt' => $comment->getCreatedAt()->getTimestamp()
                ];
            }
        }

        return $result;
    }


    protected function makeTagResponse(Tag $tag)
    {
        return [
            'id' => $tag->getId(),
            'text' => $tag->getText(),
            'taggedCount' => $tag->getTaggedCount()
        ];
    }

    protected function makeUserResponse(User $user = null)
    {
        return empty($user) ? null : [
            'id' => $user->getId(),
            'name' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'rating' => $user->getRating(),
            'ideasCount' => $user->getIdeasCount(),
            'avatar' => $user->getAvatar() ?: '/img/davatar.jpg',
            'isTmp' => $user->isTmp(),
            'registeredAt' => $user->getRegisteredAt()->getTimestamp()
        ];
    }
} 