<?php namespace CodeIdeas\Core;

use CodeIdeas\Core\Exception\AbortJsonException;


class Application extends \Silex\Application
{
    public function __construct(array $values = [])
    {
        parent::__construct($values);

        $this->error(function(AbortJsonException $e) {
            return $this->json($e->getData(), $e->getStatus(), $e->getHeaders());
        });
    }

    public function abortJson(array $data, $status = 200, array $headers = [])
    {
        throw new AbortJsonException($data, $status, $headers);
    }
} 