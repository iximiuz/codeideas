<?php namespace CodeIdeas\Core;


use CodeIdeas\Entity\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Auth
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function ensureTokenAccess(Request $request)
    {
        $token = $request->get('token');
        if (empty($token)) {
            return new Response('Auth required', 401);
        }

        /** @var Session $session */
        $session = $this->app['orm.em']->find('CodeIdeas\Entity\Session', $token);
        if (empty($session)/* || $session->getUpdatedAt() todo: check token expiration here */) {
            return new Response('Session expired', 403);
        }

        $user = $session->getUser();
        if (empty($user)) {
            return new Response('No such user', 403);
        }

        $this->app['user'] = $user;
    }
} 