<?php namespace CodeIdeas\Core\Exception;

use CodeIdeas\Exception\AppException;

class AbortJsonException extends AppException
{
    private $data;
    private $status;
    private $headers;


    public function __construct(array $data, $status, array $headers)
    {
        $this->data = $data;
        $this->status = $status;
        $this->headers = $headers;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getStatus()
    {
        return $this->status;
    }
} 