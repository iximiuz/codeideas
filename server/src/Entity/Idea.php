<?php namespace CodeIdeas\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="ideas")
 */
class Idea
{
    const MAX_DESCRIPTION_SIZE = 65536;
    const MAX_PROFIT_SIZE = 65536;
    const MAX_TITLE_SIZE = 128;
    const MAX_TAGS_COUNT = 5;

    const IMPL_TIME_HOURS = 1000;
    const IMPL_TIME_DAYS = 2000;
    const IMPL_TIME_WEEKS = 3000;
    const IMPL_TIME_MONTHS = 4000;
    const IMPL_TIME_GE_HALF_OF_YEAR = 5000;

    const TEAM_TYPE_MEDIUM = 5000;


    /** @Id @Column(type="integer") @GeneratedValue */
    private $id;

    /** @Column(type="string") */
    private $title;

    /** @Column(type="string") */
    private $description;

    /** @Column(type="string") */
    private $profit;

    /** @Column(type="integer", name="impl_time") */
    private $implTime;

    /** @Column(type="integer", name="team_type") */
    private $teamType;

    /** @Column(type="float") */
    private $rating = 0.0;

    /**
     * @OneToMany(targetEntity="Comment", mappedBy="idea")
     * @var Comment[]
     */
    private $comments;

    /** @Column(type="integer", name="comments_cnt") */
    private $commentsCount = 0;

    /** @Column(type="integer", name="likes_cnt") */
    private $likesCount = 0;

    /** @Column(type="integer", name="want_to_impl_cnt") */
    private $wantToImplementCount = 0;

    /** @Column(type="datetime", name="created_at") */
    private $createdAt;

    private $isLiked = false;

    private $isWantedToImpl = false;

    /**
     * @OneToOne(targetEntity="User")
     * @var User
     */
    private $user;

    /**
     * @ManyToMany(targetEntity="Tag")
     * @JoinTable(name="ideas_tags",
     *      joinColumns={@JoinColumn(name="idea_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     **/
    private $tags;


    public function __construct(
        $user,
        $title,
        $description,
        $profit,
        $implementationTime = null,
        $teamType = null,
        $createdAt = null
    ) {
        $this->user = $user;
        $this->title = $title;
        $this->description = $description;
        $this->profit = $profit;
        $this->teamType = $teamType;
        $this->implTime = $implementationTime;
        $this->createdAt = new \DateTime($createdAt);

        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function getCommentsCount()
    {
        return $this->commentsCount;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getImplementationTime()
    {
        return $this->implTime;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getLikesCount()
    {
        return $this->likesCount;
    }

    public function getProfit()
    {
        return $this->profit;
    }

    /**
     * @return Tag[]|ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function getTeamType()
    {
        return $this->teamType;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getWantToImplementCount()
    {
        return $this->wantToImplementCount;
    }

    public function isLiked()
    {
        return $this->isLiked;
    }

    public function isWantedToImplement()
    {
        return $this->isWantedToImpl;
    }

    public function like()
    {
        $this->likesCount++;
        $this->isLiked = true;
    }

    public function markAsLiked()
    {
        $this->isLiked = true;
    }

    public function markAsWantedToImplement()
    {
        $this->isWantedToImpl = true;
    }

    public function noLongerLike()
    {
        $this->likesCount--;
        if (0 > $this->likesCount) {
            assert(false);
            $this->likesCount = 0;
        }

        $this->isLiked = false;
    }

    public function noLongerWantToImplement()
    {
        $this->wantToImplementCount--;
        if (0 > $this->wantToImplementCount) {
            assert(false);
            $this->wantToImplementCount = 0;
        }

        $this->isWantedToImpl = false;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setImplementationTime($time)
    {
        $this->implTime = $time;
    }

    public function setLikesCount($count)
    {
        assert('$count >= 0');
        $this->likesCount = $count > 0 ? $count : 0;
    }

    public function setProfit($profit)
    {
        $this->profit = $profit;
    }

    public function setTeamType($teamType)
    {
        $this->teamType = $teamType;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function setWantToImplementCount($count)
    {
        assert('$count >= 0');
        $this->wantToImplementCount = $count > 0 ? $count : 0;
    }

    public function wantToImplement()
    {
        $this->wantToImplementCount++;
        $this->isWantedToImpl = true;
    }
} 