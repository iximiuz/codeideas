<?php namespace CodeIdeas\Entity;

use CodeIdeas\Entity\Exception\BadTagFormatException;

/**
 * @Entity @Table(name="tags")
 */
class Tag
{
    /** @Id @Column(type="integer") @GeneratedValue */
    private $id;

    /** @Column(type="string") */
    private $text;

    /**
     * @OneToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     */
    private $owner;

    /** @Column(type="integer", name="tagged_cnt") */
    private $taggedCount;

    /** @Column(type="datetime", name="created_at") */
    private $createdAt;


    public static function isValidTag($tagText)
    {
        return preg_match('/^[a-zA-Z\d$.\-+#]{1,24}$/', $tagText);
    }


    public function __construct($text, $taggedCount = 0, $createdAt = null)
    {
        if (!self::isValidTag($text)) {
            throw new BadTagFormatException();
        }

        $this->text = $text;
        $this->taggedCount = $taggedCount;
        $this->createdAt = new \DateTime($createdAt);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function getTaggedCount()
    {
        return $this->taggedCount;
    }

    public function getText()
    {
        return $this->text;
    }
} 