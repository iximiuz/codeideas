<?php namespace CodeIdeas\Entity;

/**
 * @Entity @Table(name="users")
 */
class User
{
    /** @Id @Column(type="integer") @GeneratedValue */
    private $id;

    /** @Column(type="string", name="first_name") */
    private $firstName;

    /** @Column(type="string", name="last_name") */
    private $lastName;

    /** @Column(type="string") */
    private $email;

    /** @Column(type="string") */
    private $avatar;

    /** @Column(type="integer", name="ideas_cnt") */
    private $ideasCount = 0;

    /** @Column(type="integer", name="is_temp") */
    private $isTmp = false;

    /** @Column(type="integer") */
    private $rating = 0;

    /** @Column(type="string", name="pwd_hash") */
    private $passwordHash;

    /** @Column(type="datetime", name="registered_at") */
    private $registeredAt;


    public static function hashPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT, ['salt' => 'I have to use random salt!']);
    }


    public function __construct($firstName, $lastName, $email = null, $avatar = null, $rating = 0, $registeredAt = null)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->avatar = $avatar;
        $this->rating = $rating;
        $this->registeredAt = new \DateTime($registeredAt);
    }

    public function decrRating($amount)
    {
        assert('$amount >= 0');
        $this->rating = max(0, $this->rating - $amount);
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIdeasCount()
    {
        return $this->ideasCount;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt()
    {
        return $this->registeredAt;
    }

    public function incrRating($amount)
    {
        assert('$amount >= 0');
        $this->rating += max(0, $amount);
    }

    public function isTmp()
    {
        return $this->isTmp;
    }

    public function markAsTmp($val = true)
    {
        $this->isTmp = $val;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function setPasswordHash($password)
    {
        $this->passwordHash = self::hashPassword($password);
    }
} 