<?php namespace CodeIdeas\Entity;
use CodeIdeas\Entity\Exception\BadCommentFormatException;

/**
 * @Entity @Table(name="comments")
 */
class Comment
{
    const MIN_LENGTH = 8;
    const MAX_LEN = 32768;


    /** @Id @Column(type="integer") @GeneratedValue */
    private $id;

    /** @Column(type="string") */
    private $text;

    /**
     * @OneToOne(targetEntity="User", fetch="EAGER")
     * @var User
     */
    private $user;

    /**
     * @OneToOne(targetEntity="Idea", fetch="EAGER")
     * @var Idea
     */
    private $idea;

    /**
     * @OneToOne(targetEntity="Comment")
     * @JoinColumn(name="parent_id", referencedColumnName="id")
     * @var Comment
     */
    private $parentComment;

    /** @Column(type="string") */
    private $mpath = '';

    /** @Column(type="integer") */
    private $deep = 0;

    /** @Column(type="datetime", name="created_at") */
    private $createdAt;


    public function __construct($text, User $author, Idea $idea, Comment $parentComment = null, $createdAt = null)
    {
        if (!is_string($text) || self::MAX_LEN < strlen($text) || self::MIN_LENGTH > strlen($text)) {
            throw new BadCommentFormatException();
        }

        $this->text = $text;
        $this->user = $author;
        $this->idea = $idea;
        $this->parentComment = $parentComment;
        $this->createdAt = new \DateTime($createdAt);
    }

    public function getAuthor()
    {
        return $this->user;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getDeep()
    {
        return $this->deep;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIdea()
    {
        return $this->idea;
    }

    public function getMPath()
    {
        return $this->mpath;
    }

    /**
     * @return Comment
     */
    public function getParentComment()
    {
        return $this->parentComment;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }
} 