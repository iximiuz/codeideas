<?php namespace CodeIdeas\Entity;

use CodeIdeas\Entity\Exception\EntityException;

/**
 * @Entity @Table(name="social_profiles")
 */
class SocialProfile
{
    /** @Id @Column(type="integer") @GeneratedValue */
    private $id;

    /** @Column(type="string") */
    private $network;

    /** @Column(type="string") */
    private $profile;

    /** @Column(type="string") */
    private $uid;

    /** @Column(type="string") */
    private $identity;

    /** @Column(type="datetime", name="created_at") */
    private $createdAt;

    /**
     * @OneToOne(targetEntity="User")
     * @var User
     */
    private $user;


    /**
     * @param array $data
     * @return SocialProfile
     * @throws Exception\EntityException
     */
    public static function create(array $data)
    {
        $result = new self();

        if (empty($data['network']) || empty($data['uid']) || empty($data['identity'])) {
            throw new EntityException('Missed required params.');
        }

        $result->profile = isset($data['profile']) ? $data['profile'] : null;
        $result->network = $data['network'];
        $result->identity = $data['identity'];
        $result->uid = $data['uid'];
        $result->createdAt = new \DateTime();

        return $result;
    }

    public function getId()
    {
        return $this->id;
    }

    public function addLinkedUser(User $user)
    {
        $this->user = $user;
    }

    public function getLinkedUser()
    {
        return $this->user;
    }
} 