<?php namespace CodeIdeas\Entity;


/**
 * @Entity @Table(name="user_rating_log")
 */
class UserRatingLog
{
    /** @Id @Column(type="integer") @GeneratedValue */
    private $id;

    /**
     * @OneToOne(targetEntity="User")
     * @var User
     */
    private $user;

    /** @Column(type="integer", name="event_type") */
    private $eventType;

    /** @Column(type="integer", name="rating_amount") */
    private $ratingAmount;

    /** @Column(type="string") */
    private $data = '';

    /** @Column(type="datetime", name="created_at") */
    private $createdAt;


    public function __construct(User $user, $eventType, $ratingAmount, $createdAt = null)
    {
        $this->user = $user;
        $this->eventType = $eventType;
        $this->ratingAmount = $ratingAmount;
        $this->createddAt = new \DateTime($createdAt);
    }

    public function getAmount()
    {
        return $this->ratingAmount;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getEventType()
    {
        return $this->eventType;
    }

    public function setCustomData($data)
    {
        $this->data = $data;
    }
} 