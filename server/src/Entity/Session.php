<?php namespace CodeIdeas\Entity;

/**
 * @Entity @Table(name="sessions")
 */
class Session
{
    /** @Id @Column(type="string") */
    private $id;

    /** @Column(type="datetime", name="created_at") */
    private $createdAt;

    /**
     * @OneToOne(targetEntity="User")
     * @var User
     */
    private $user;


    public function __construct($externalToken, User $user, $createdAt = null)
    {
        $this->id = password_hash($externalToken, PASSWORD_DEFAULT);
        $this->user = $user;
        $this->createdAt = new \DateTime($createdAt);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }
} 