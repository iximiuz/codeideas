<?php namespace CodeIdeas\Service\Rating;

use CodeIdeas\Entity\User;
use CodeIdeas\Entity\UserRatingLog;
use CodeIdeas\Service\Rating\Exception\RatingServiceException;
use Doctrine\ORM\EntityManager;

class RatingService
{
    const RATING_REGISTRATION = 20000;
    const RATING_INCR_IDEA_LIKE = 20001;
    const RATING_INCR_IDEA_WANT_TO_IMPL = 20002;
    const RATING_INCR_TAG_USED = 20004;


    private $entityManager;

    private $awards = [
        self::RATING_REGISTRATION => 10,
        self::RATING_INCR_IDEA_LIKE => 50,
        self::RATING_INCR_IDEA_WANT_TO_IMPL => 100,
        self::RATING_INCR_TAG_USED => 20,
    ];


    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function ratingAward(User $user, $awardType, array $customData = [])
    {
        if (!isset($this->awards[$awardType])) {
            throw new RatingServiceException('Unknown award type [' . $awardType . ']');
        }

        $user->incrRating($this->awards[$awardType]);

        $logEntry = new UserRatingLog($user, $awardType, $this->awards[$awardType]);
        if (!empty($customData)) {
            asort($customData);
            $logEntry->setCustomData(json_encode($customData));
        }

        $this->entityManager->persist($user);
        $this->entityManager->persist($logEntry);
    }

    public function ratingReward(User $user, $rewardType, array $customData = [])
    {
        if (!isset($this->awards[$rewardType])) {
            throw new RatingServiceException('Unknown award type [' . $rewardType . ']');
        }

        $user->decrRating($this->awards[$rewardType]);

        $queryParams = ['user' => $user->getId(), 'eventType' => $rewardType];
        if (!empty($customData)) {
            asort($customData);
            $queryParams['data'] = json_encode($customData);
        }

        $logEntry = $this->entityManager->getRepository('CodeIdeas\Entity\UserRatingLog')->findOneBy($queryParams);

        $this->entityManager->persist($user);
        if ($logEntry) {
            $this->entityManager->remove($logEntry);
        }
    }
}