<?php namespace CodeIdeas\Service\Voting;

use CodeIdeas\Service\Voting\Exception\ReVoteBanException;
use CodeIdeas\Service\Voting\Exception\VoteNotFoundException;
use CodeIdeas\Service\Voting\Exception\VotingServiceException;
use Psr\Log\LoggerInterface;

class VotingService
{
    const ERR_CODE_REVOTE_BAN     = 10001;
    const ERR_CODE_VOTE_NOD_FOUND = 10002;


    private $clientId;

    private $host;

    private $port;

    private $logger;


    public function __construct($host, $port, $clientId, LoggerInterface $logger)
    {
        $this->host = $host;
        $this->port = $port;
        $this->clientId = $clientId;

        $this->logger = $logger;
    }

    public function getVotes($voterId, $voteObjectType, array $tags = [], $limit = 100, $offset = 0)
    {
        return $response = $this->write(
            $this->makeGetVoterVotesCommand($voterId, $voteObjectType, $tags, $limit, $offset)
        );
    }

    public function getVotesCount($voteObjectType, array $voteObjectIds, array $tags = [], $voterId = null, $version = 1)
    {
        if (empty($voteObjectIds)) {
            return [];
        }

        return $this->write(
            $this->makeGetVotesCountCommand($voteObjectType, $voteObjectIds, $tags, $voterId, $version)
        );
    }

    public function removeVote($voteObjectType, $voteObjectId, $voterSubjectId, $value, $tag = null, $version = 1)
    {
        $response = $this->write(
            $this->makeRemoveVoteCommand(
                $voteObjectType, $voteObjectId, $voterSubjectId, $value, $tag, $version
            )
        );

        if (empty($response['result'])) {
            if (!empty($response['code']) && self::ERR_CODE_VOTE_NOD_FOUND == $response['code']) {
                $this->logger->warning('Vote not found [' . var_export(func_get_args(), 1) . ']');
                throw new VoteNotFoundException();
            }

            $this->logger->error(
                'Can not remove vote [' . var_export($response, 1) . '] [' . var_export(func_get_args(), 1) . ']'
            );
            throw new VotingServiceException('Can remove save vote. Unexpected error.');
        }
    }

    public function vote($voteObjectType, $voteObjectId, $voterSubjectId, $value, $createdAt, $tag = null, $version = 1)
    {
        $response = $this->write(
            $this->makeVoteCommand($voteObjectType, $voteObjectId, $voterSubjectId, $value, $createdAt, $tag, $version)
        );

        if (empty($response['result'])) {
            if (!empty($response['code']) && self::ERR_CODE_REVOTE_BAN == $response['code']) {
                $this->logger->warning('ReVote [' . var_export(func_get_args(), 1) . ']');
                throw new ReVoteBanException();
            }

            $this->logger->error(
                'Can not save vote [' . var_export($response, 1) . '] [' . var_export(func_get_args(), 1) . ']'
            );
            throw new VotingServiceException('Can not save vote. Unexpected error.');
        }
    }

    /******************************************************************************************************************/

    private function makeGetVoterVotesCommand($voterId, $voteObjectType, $tags, $limit, $offset)
    {
        $body = [
            'voterId' => $voterId,
            'voteeType' => $voteObjectType,
            'limit' => $limit,
            'offset' => $offset
        ];

        if (!empty($tags)) {
            $body['tags'] = $tags;
        }

        $body = json_encode($body);

        $header = ['c' => 'get_voter_votes', 'bl' => strlen($body)];

        return json_encode($header) . $body;
    }

    private function makeGetVotesCountCommand($voteObjectType, array $voteObjectIds, array $tags = [], $voterId = null, $version = 1)
    {
        $body = [
            'voteeType' => $voteObjectType,
            'voteeIds' => $voteObjectIds,
            'v' => $version,
            'client' => $this->clientId
        ];

        if (!empty($tags)) {
            $body['tags'] = $tags;
        }

        if (!empty($voterId)) {
            $body['voterId'] = $voterId;
        }

        $body = json_encode($body);

        $header = ['c' => 'get_votes_count', 'bl' => strlen($body)];

        return json_encode($header) . $body;
    }

    private function makeRemoveVoteCommand($voteObjectType, $voteObjectId, $voterSubjectId, $value, $tag, $version)
    {
        $body = $this->makeVoteCommandBodyBase($voteObjectType, $voteObjectId, $voterSubjectId, $value, $tag, $version);
        $body = json_encode($body);

        $header = ['c' => 'remove_vote', 'bl' => strlen($body)];

        return json_encode($header) . $body;
    }

    private function makeVoteCommand($voteObjectType, $voteObjectId, $voterSubjectId, $value, $createdAt, $tag = null, $version = 1)
    {
        $body = $this->makeVoteCommandBodyBase($voteObjectType, $voteObjectId, $voterSubjectId, $value, $tag, $version);
        $body['createdAt'] = $createdAt;

        $body = json_encode($body);

        $header = ['c' => 'vote', 'bl' => strlen($body)];

        return json_encode($header) . $body;
    }

    private function makeVoteCommandBodyBase($voteObjectType, $voteObjectId, $voterSubjectId, $value, $tag = null, $version = 1)
    {
        $body = [
            'voteeType' => $voteObjectType,
            'voteeId' => $voteObjectId,
            'voterId' => $voterSubjectId,
            'value' => $value,
            'v' => $version,
            'client' => $this->clientId,
        ];

        if (!empty($tag)) {
            $body['tag'] = $tag;
        }

        return $body;
    }

    private function write($text)
    {
        $fp = @stream_socket_client('tcp://' . $this->host . ':' . $this->port, $errno, $errstr, 5);
        if (false === $fp) {
            throw new VotingServiceException("Error during socket connect [($errno) $errstr]");
        }

        if (false === @fwrite($fp, $text)) {
            throw new VotingServiceException("Error during socket write.");
        }

        $response = '';
        while (!@feof($fp)) {
            if (false === ($retVal = @fgets($fp, 1024))) {
                throw new VotingServiceException("Error during socket read.");
            }

            $response .= $retVal;
        }
        @fclose($fp);

        if (empty($response) || (null === ($response = json_decode($response, true)))) {
            throw new VotingServiceException("Error during read from socket [$response]");
        }

        return $response;
    }
} 