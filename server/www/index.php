<?php

require_once __DIR__ . '/../vendor/autoload.php';

use CodeIdeas\Controller\Provider\ResourceControllerProvider;
use CodeIdeas\Core\Application as CodeIdeasApp;
use CodeIdeas\Core\Auth;
use CodeIdeas\Service\Rating\RatingService;
use CodeIdeas\Service\Voting\VotingService;
use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;


$app = new CodeIdeasApp();
$app['debug'] = true;

ExceptionHandler::register($app['debug']);
ErrorHandler::register();


// DI & configuration
$app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__ . '/../config/config.php'));
if ($app['debug']) {
    $app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__ . '/../config/config.debug.php'));
}

if (file_exists(__DIR__ . '/../config/config.local.php')) {
    $app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__ . '/../config/config.local.php'));
}

$app->register(
    new Silex\Provider\MonologServiceProvider(),
    [
        'monolog.logfile' => $app['debug']
                ? __DIR__ . '/../log/development.log'
                : __DIR__ . '/../log/production.log'
    ]
);

$app->register(new DoctrineServiceProvider(), [
    'db.options' => [
        'driver' => 'pdo_mysql',
        'host'     => 'localhost',
        'user'     => 'root',
        'password' => 'qwerty',
        'dbname'   => 'codeideasdb',
        'driverOptions' => [
            1002 => 'SET NAMES utf8'
        ]
    ],
]);

$doctrineOrmOptions = [
    'orm.em.options' => [
        'mappings' => [
            [
                'type' => 'annotation',
                'namespace' => 'CodeIdeas\Entity',
                'path' => __DIR__ . '/../src/Entity',
            ],
        ],
    ],
];

if (empty($app['debug']) && extension_loaded('memcache')) {
    $doctrineOrmOptions['orm.default_cache'] = ['driver' => 'memcache', 'host' => 'localhost', 'port' => 11211];
}

$app->register(new DoctrineOrmServiceProvider(), $doctrineOrmOptions);

//$app['orm.em']->getConnection()
//    ->getConfiguration()
//    ->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());

$app['auth'] = $app->share(function(CodeIdeasApp $app) {
    return new Auth($app);
});

$app['voting'] = $app->share(function(CodeIdeasApp $app) {
    return new VotingService(
        $app['config']['voting']['host'],
        $app['config']['voting']['port'],
        $app['config']['voting']['clientId'],
        $app['logger']
    );
});

$app['rating'] = $app->share(function(CodeIdeasApp $app) {
    return new RatingService(
        $app['orm.em']
    );
});

// routing
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : []);
    }
}, CodeIdeasApp::EARLY_EVENT);

$tokenAuth = function(Request $request) use ($app) { $app['auth']->ensureTokenAccess($request); };
$strictTokenAuth = function(Request $request) use ($app) { return $app['auth']->ensureTokenAccess($request); };

$app->mount(
    '/auth/',
    new ResourceControllerProvider(
        CodeIdeas\Controller\AuthController::class,
        null,
        [],
        [
            ResourceControllerProvider::METHOD_INDEX,
            ResourceControllerProvider::METHOD_CREATE,
            ResourceControllerProvider::METHOD_DELETE,
        ]
    )
);

$app->mount(
    '/comments/',
    new ResourceControllerProvider(
        CodeIdeas\Controller\CommentsController::class
    )
);

$app->mount(
    '/ideas/',
    new ResourceControllerProvider(
        CodeIdeas\Controller\IdeasController::class,
        $tokenAuth
    )
);

$app->mount(
    '/ideas/{ideaId}/comments/',
    new ResourceControllerProvider(
        CodeIdeas\Controller\CommentsController::class,
        $strictTokenAuth
    )
);

$app->mount(
    '/ideas/{ideaId}/votes/{tag}',
    new ResourceControllerProvider(
        CodeIdeas\Controller\LikesController::class,
        $strictTokenAuth,
        ['tag' => '^(like|impl)$', 'id' => '^my$']
    )
);

$app->mount(
    '/users/{userId}/rating-log/',
    new ResourceControllerProvider(
        CodeIdeas\Controller\RatingLogController::class,
        $tokenAuth,
        [],
        [
            ResourceControllerProvider::METHOD_INDEX,
        ]
    )
);

$app->mount(
    '/tags/',
    new ResourceControllerProvider(
        CodeIdeas\Controller\TagsController::class,
        $tokenAuth,
        [],
        [
            ResourceControllerProvider::METHOD_INDEX,
        ]
    )
);

$app->mount(
    '/users/',
    new ResourceControllerProvider(
        CodeIdeas\Controller\UsersController::class,
        $tokenAuth,
        [],
        [
            ResourceControllerProvider::METHOD_INDEX,
            ResourceControllerProvider::METHOD_SHOW,
        ]
    )
);

$app->mount(
    '/users/{userId}/tags/',
    new ResourceControllerProvider(
        CodeIdeas\Controller\TagsController::class,
        $tokenAuth
    )
);

// start!
$app->run();