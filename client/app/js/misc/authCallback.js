(function(window) {
    'use strict';

    window.authCallback = function(externalToken) {
        var auth = angular.element('*[ng-app]').injector().get('auth');

        $.post(
            '/api/auth/?targetHost=' + encodeURIComponent(location.toString()) +
            '&externalToken=' + externalToken + (auth.getToken() ? '&token=' + auth.getToken() : ''),
            function(data) {
                if (data.user) {
                    auth.login(data);
                } else {
                    console.log(data);
                    alert('Извините, произошла ошибка :(');
                }
            }
        );
    };
})(window);