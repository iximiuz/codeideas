'use strict';

var services = angular.module('ciApp.services');

services.factory('auth', ['$rootScope', '$http', '$location', '$route', 'CONFIG', function($rootScope, $http, $location, $route, CONFIG) {
    var user;

    function onAuthError(self, fnOnError) {
        return function(response, status) {
            self.logout();
            if (fnOnError) {
                fnOnError(response, status)
            }

            console.log('auth failure', arguments);
        }
    }

    function onAuthSuccess(fnOnSuccess, self) {
        return function(authData) {
            if (authData.user) {
                self.login(authData);
                if (fnOnSuccess) {
                    fnOnSuccess();
                }
            } else {
                self.logout();
            }
        };
    }

    return {
        getToken: function() {
            return user ? user.token : localStorage.getItem('auth_token');
        },

        getUser: function() {
            return user;
        },

        getUserId: function()  {
            return user ? user.id : null;
        },

        login: function(authData) {
            var prevUser = user;

            user = authData.user;
            localStorage.setItem('auth_token', user.token);

            var path = $location.path();
            if ({'/login': 1, '/login/classic': 1, '/register': 1}[path]) {
                path = '/ideas'; // todo: use prev route
            } else if (prevUser && prevUser.isTmp && ('/users/' + prevUser.id) === path) {
                // связка аккаунтов, пока хаком. Перенаправление на новый профиль
                path = '/users/' + user.id
            }

            $location.path(path);
            $route.reload();
        },

        logout: function() {
            localStorage.removeItem('auth_token');
            user = null;
            // todo: send DELETE request to destroy session on server side
        },

        tryLogin: function(fnOnSuccess) {
            var authToken = localStorage.getItem('auth_token'), self = this;
            if (authToken) {
                $http.get(CONFIG.apiHost + '/auth/?token=' + authToken)
                    .success(onAuthSuccess(fnOnSuccess, this)).error(onAuthError(this));
            } else {
                setTimeout(function() { $rootScope.$apply(); }, 0);
            }
        },

        tryLoginClassic: function(email, password, fnOnSuccess, fnOnError) {
            $http.post(
                CONFIG.apiHost + '/auth/?' + (this.getUser() && this.getUser().isTmp ? 'token=' + this.getUser().token : 'z=1'),
                {email: email, password: password}
            ).success(onAuthSuccess(fnOnSuccess, this)).error(onAuthError(this, fnOnError));
        }
    };
}]);