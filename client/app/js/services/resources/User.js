'use strict';

var resources = angular.module('ciApp.services.resources');

resources.factory('User', ['$resource', 'CONFIG', 'auth', function($resource, CONFIG, auth) {
    return $resource(
        CONFIG.apiHost + '/users/:id?token=:token',
        {token: function() { return auth.getToken(); }},
        {

        }
    );
}]);
