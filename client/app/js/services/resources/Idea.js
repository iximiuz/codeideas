'use strict';

var resources = angular.module('ciApp.services.resources');

resources.factory('Idea', ['$resource', 'CONFIG', 'auth', function($resource, CONFIG, auth) {
    return $resource(
        CONFIG.apiHost + '/ideas/:id?token=:token',
        {id: '@id', token: function() { return auth.getToken(); }},
        {
            'update': {
                method: 'PUT'
            },

            like: {
                method: 'POST',
                params: {},
                url: CONFIG.apiHost + '/ideas/:id/votes/like/?token=:token'
            },

            noLongerLike: {
                method: 'DELETE',
                url: CONFIG.apiHost + '/ideas/:id/votes/like/my?token=:token'
            },

            noLongerWantToImplement: {
                method: 'DELETE',
                url: CONFIG.apiHost + '/ideas/:id/votes/impl/my?token=:token'
            },

            wantToImplement: {
                method: 'POST',
                params: {},
                url: CONFIG.apiHost + '/ideas/:id/votes/impl/?token=:token'
            }
        }
    );
}]);
