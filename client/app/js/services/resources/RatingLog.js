'use strict';

var resources = angular.module('ciApp.services.resources');

resources.factory('RatingLog', ['$resource', 'CONFIG', 'auth', function($resource, CONFIG, auth) {
    return $resource(
        CONFIG.apiHost + '/users/:userId/rating-log/?token=:token',
        {token: function() { return auth.getToken(); }},
        {

        }
    );
}]);
