'use strict';

var resources = angular.module('ciApp.services.resources');

resources.factory('Comment', ['$resource', 'CONFIG', 'auth', function($resource, CONFIG, auth) {
    return $resource(
        CONFIG.apiHost + '/ideas/:ideaId/comments/:id?token=:token',
        {token: function() { return auth.getToken(); }},
        {
            latest: {url: CONFIG.apiHost + '/comments/?token=:token&q=latest', isArray: true},
            getUserComments: {url: CONFIG.apiHost + '/comments/?token=:token&userId=:userId', isArray: true}
        }
    );
}]);
