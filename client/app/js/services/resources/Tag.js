'use strict';

var resources = angular.module('ciApp.services.resources');

resources.factory('Tag', ['$resource', 'CONFIG', 'auth', function($resource, CONFIG, auth) {
    return $resource(
        CONFIG.apiHost + '/tags/?token=:token',
        {token: function() { return auth.getToken(); }},
        {
            getUserTags: {url: CONFIG.apiHost + '/users/:userId/tags/?token=:token', isArray: true}
        }
    );
}]);
