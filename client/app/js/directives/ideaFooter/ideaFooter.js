'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('ideaFooter', ['auth', function(auth) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            idea: '='
        },
        link: function(scope, el, attrs) {
            scope.isListItem = attrs.isListItem;
            scope.canEdit = function() {
                return scope.idea.user && scope.idea.user.id == auth.getUserId();
            };

            scope.wantToImplement = function() {
                if (auth.getUser()) {
                    if (scope.idea.isWantedToImpl) {
                        scope.idea.$noLongerWantToImplement(function() {
                            scope.idea.createdAt = moment(scope.idea.createdAt, 'X').format('MMMM D, YYYY');
                        });
                    } else {
                        scope.idea.$wantToImplement(function() {
                            scope.idea.createdAt = moment(scope.idea.createdAt, 'X').format('MMMM D, YYYY');
                        });
                    }
                } else {
                    alert('Авторизуйтесь, пожалуйста (:')
                }
            };

            scope.wantToImplementTitle = function() {
                if (!scope.idea.wantToImplementCount) {
                    return '';
                }

                var result = 'Хотят реализовать эту идею: ';
                if (scope.idea.isWantedToImpl) {
                    if (scope.idea.wantToImplementCount > 1) {
                        result += ' Вы и еще ' + (scope.idea.wantToImplementCount - 1) + ' чел.';
                    } else {
                        result = 'Пока только Вы хотите реализовать эту идею';
                    }
                } else {
                    result += scope.idea.wantToImplementCount + ' чел.';
                }

                return result;
            };

            scope.wantToImplementText = function() {
                return 'Хочу реализовать' + (scope.idea.wantToImplementCount ? ' (' + scope.idea.wantToImplementCount + ')' : '');
            };
        },
        templateUrl: 'js/directives/ideaFooter/idea-footer.html'
    };
}]);
