'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('popularTags', ['Tag', function(Tag) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            scope.tags = Tag.query({q: 'popular'});
        },
        templateUrl: 'js/directives/popularTags/popular-tags.html'
    };
}]);
