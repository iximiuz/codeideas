'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('idea', ['Idea', 'auth', '$routeParams', function(Idea, auth, $routeParams) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            scope.idea = Idea.get({id: $routeParams.id, token: auth.getToken()}, function() {
                scope.idea.createdAt = moment(scope.idea.createdAt, 'X').format('MMMM D, YYYY');

                if (scope.idea.comments && scope.idea.comments.length) {
                    for (var i = 0, l = scope.idea.comments.length; i < l; i++) {
                        scope.idea.comments[i].createdAt = moment(scope.idea.comments[i].createdAt, 'X').format('MMMM D, YYYY [в] hh:mm');
                    }
                }
            });

            scope.implTimeText = function(idea) {
                var map = {
                    1000: 'Несколько часов',
                    2000: 'Несколько дней',
                    3000: 'Несколько недель',
                    4000: 'Несколько месяцев',
                    5000: 'Более полугода'
                };

                return map[idea.implementationTime] + ' ' + (idea.teamType ? 'командной разработки' : 'одиночной разработки');
            };
        },
        templateUrl: 'js/directives/idea/idea.html'
    };
}]);
