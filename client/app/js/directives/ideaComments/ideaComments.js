'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('ideaComments', ['Comment', 'auth', function(Comment, auth) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            idea: '='
        },
        link: function(scope, el, attrs) {
            var replayForms = {};

            scope.canComment = function() {
                return !!auth.getUser()
            };

            scope.comment = {};
            scope.commentReplay = {};

            scope.addComment = function() {
                scope.comment.ideaId = scope.idea.id;
                scope.comment.userId = auth.getUser().id;
                var comment = new Comment(scope.comment);

                comment.$save({ideaId: scope.idea.id}, function() {
                    scope.comment.text = '';

                    comment.createdAt = moment(comment.createdAt, 'X').format('MMMM D, YYYY [в] hh:mm');

                    scope.idea.comments = scope.idea.comments || [];
                    scope.idea.comments.push(comment);
                });
            };

            scope.addCommentReplay = function(parentCommentId) {
                scope.commentReplay.ideaId = scope.idea.id;
                scope.commentReplay.userId = auth.getUser().id;
                scope.commentReplay.parentId = parentCommentId;
                var commentReplay = new Comment(scope.commentReplay);

                commentReplay.$save({ideaId: scope.idea.id}, function() {
                    commentReplay.createdAt = moment(commentReplay.createdAt, 'X').format('MMMM D, YYYY [в] hh:mm');

                    replayForms[parentCommentId] = false;

                    scope.idea.comments = scope.idea.comments || [];
                    scope.idea.comments.push(commentReplay);
                });
            };

            scope.replayIsShowed = function(commentId) {
                return !!replayForms[commentId];
            };

            scope.showReplay = function(commentId) {
                replayForms[commentId] = true;
            };
        },
        templateUrl: 'js/directives/ideaComments/idea-comments.html'
    };
}]);
