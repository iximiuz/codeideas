'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('usersList', ['$location', 'User', function($location, User) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            scope.tabs = [
                {text: 'По рейтингу', sort: 'rating'},
                {text: 'Новые', sort: 'registration'}
            ];

            var sort = $location.search().sort,
                curTab,
                limit = 20;

            if (sort) {
                for (var i = 0, l = scope.tabs.length; i < l; i++) {
                    if (scope.tabs[i].sort === sort) {
                        curTab = scope.tabs[i];
                        break;
                    }
                }
            }

            if (!curTab) {
                curTab = scope.tabs[0];
            }

            var page = $location.search().page;
            scope.hasNext = false;
            scope.hasPrev = false;

            curTab.active = true;

            function loadUsers() {
                $location.search('page', page && page > 1 ? page: void 0);
                $location.search('sort', 'rating' !== curTab.sort ? curTab.sort : void 0);

                var params = {sort: curTab.sort, page: page, limit: limit};

                scope.users = User.query(params, function() {
                    scope.hasPrev = !!page && 1 < page;
                    scope.hasNext = scope.users.length >= limit;
                });
            }

            loadUsers();

            scope.registeredAt = function(user) {
                return moment(user.registeredAt, 'X').fromNow().replace(' назад', '');
            };

            scope.activateTab = function(tab) {
                scope.hasPrev = false;
                scope.hasNext = false;

                if (curTab == tab) {
                    return;
                }

                curTab.active = false;
                curTab = tab;
                curTab.active = true;

                page = void 0;

                loadUsers();
            };

            scope.loadMore = function(direction) {
                if ('next' === direction) {
                    page = page || 1;
                    page++;
                } else {
                    if (!page || page <= 1) {
                        return;
                    }

                    page--;
                }

                loadUsers();
            };
        },
        templateUrl: 'js/directives/usersList/users-list.html'
    };
}]);
