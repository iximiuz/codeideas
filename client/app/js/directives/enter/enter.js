'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('enter', ['auth', '$location', '$rootScope', function(auth, $location, $rootScope) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            var $body = jQuery('body');
            if (!jQuery('#ulogin-script').length) {
                $body.prepend('<script id="ulogin-callback" src="js/misc/authCallback.js"></script>');
                $body.prepend('<script id="ulogin-script" src="//ulogin.ru/js/ulogin.js"></script>');
            }

            function initUloginWidget() {
                if ((!jQuery('#' + scope.uloginId).length || !jQuery('#ulogin-script').length) && counter++ < 20) {
                    setTimeout(initUloginWidget, timeout);
                    return;
                }

                setTimeout(function() {
                    $body.prepend('<script>uLogin.customInit("' + scope.uloginId + '")</script>')
                }, 100);
            }

            scope.login = 'login' === attrs.case;
            scope.register = 'register' === attrs.case;
            scope.bindProfile = 'bind-profile' === attrs.case;
            scope.classicLogin = '/login/classic' === $location.path();

            scope.enter = {email: '', password: '', submitError: ''};

            scope.submitEnterForm = function() {
                scope.enter.submitError = '';
                scope.enter.submitInProgress = true;

                auth.tryLoginClassic(scope.enter.email, scope.enter.password, null, function(response, status) {
                    scope.enter.submitError = 409 == status
                        ? 'Пользователя не существует или неверный пароль.'
                        : 'Извините, произошла ошибка. Попробуйте еще раз.'

                    scope.enter.submitInProgress = false;
                });
            };

            scope.$on('$destroy', function() {
                if (auth.getUser() && !auth.getUser().isTmp) {
                    jQuery('#ulogin-callback').remove();
                    jQuery('#ulogin-script').remove();
                }
            });

            $rootScope.uloginNo = $rootScope.uloginNo || 0;
            $rootScope.uloginNo++;

            scope.uloginId = 'ulogin-enter-' + $rootScope.uloginNo;

            if (!scope.classicLogin) {
                var counter = 0, timeout = 250;
                setTimeout(initUloginWidget, timeout);
            }
        },
        templateUrl: 'js/directives/enter/enter.html'
    };
}]);
