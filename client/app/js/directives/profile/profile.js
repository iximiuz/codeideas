'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('profile', ['$location', '$routeParams', 'auth', 'User', function($location, $routeParams, auth, User) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            scope.user = auth.getUserId() == $routeParams.id
                ? auth.getUser()
                : User.get({id: $routeParams.id}, function() {}, function(response) {
                    if (404 == response.status) {
                        scope.user = null;
                    }
            });

            if (scope.user.id == auth.getUserId()) {
                scope.user.isI = true;
            }

            scope.registeredAt = function() {
                return moment(scope.user.registeredAt, 'X').fromNow();
            };

            scope.showBindForm = auth.getUser() && auth.getUser().isTmp && auth.getUserId() == $routeParams.id;
        },
        templateUrl: 'js/directives/profile/profile.html'
    };
}]);
