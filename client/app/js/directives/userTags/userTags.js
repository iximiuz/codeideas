'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('userTags', ['Tag', function(Tag, auth) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            user: '='
        },
        link: function(scope, el, attrs) {
            scope.tabs = [];
            scope.tags = [];
            scope.loaded = false;

            function _link() {
                scope.tabs = [
                    {text: 'Создал', active: true},
                    {text: 'Использовал', q: 'used'}
                ];

                var curTab = scope.tabs[0];

                function loadTags() {
                    scope.loaded = false;

                    scope.tags = Tag.getUserTags({userId: scope.user.id, q: curTab.q}, function() {
                        scope.loaded = true;
                    });
                }

                loadTags();

                scope.activateTab = function(tab) {
                    curTab.active = false;
                    curTab = tab;
                    curTab.active = true;

                    loadTags();
                };
            }

            if (scope.user.hasOwnProperty('$resolved') && !scope.user.$resolved) {
                scope.user.$promise.then(_link);
            } else {
                _link();
            }
        },
        templateUrl: 'js/directives/userTags/user-tags.html'
    };
}]);
