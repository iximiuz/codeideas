'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('userIdeas', ['Idea', 'auth', function(Idea, auth) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            user: '='
        },
        link: function(scope, el, attrs) {
            scope.tabs = [];
            scope.ideas = [];
            scope.loaded = false;

            function _link() {
                scope.tabs = [
                    {text: scope.user.id == auth.getUserId() ? 'Мои' : 'Его', reqParams: {userId: scope.user.id}, type: 'my', active: true},
                    {text: 'Понравившиеся', reqParams: {likedBy: scope.user.id}, type: 'liked'},
                    {text: 'На реализации', reqParams: {implBy: scope.user.id}, type: 'impl'}
                ];

                var curTab = scope.tabs[0];

                function loadIdeas() {
                    scope.hasMoreIdeas = false;
                    scope.loaded = false;

                    var params = {limit: 6};
                    if (curTab.reqParams) {
                        for (var k in curTab.reqParams) if (curTab.reqParams.hasOwnProperty(k)) {
                            params[k] = curTab.reqParams[k];
                        }
                    }

                    scope.ideas = Idea.query(params, function() {
                        if (scope.ideas.length > 5) {
                            scope.ideas.pop();
                            scope.hasMoreIdeas = true;
                        }
                        scope.loaded = true;
                    });
                }

                loadIdeas();

                scope.activateTab = function(tab) {
                    curTab.active = false;
                    curTab = tab;
                    curTab.active = true;

                    loadIdeas();
                };
            }

            if (scope.user.hasOwnProperty('$resolved') && !scope.user.$resolved) {
                scope.user.$promise.then(_link);
            } else {
                _link();
            }
        },
        templateUrl: 'js/directives/userIdeas/user-ideas.html'
    };
}]);
