'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('latestComments', ['Comment', function(Comment) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            scope.comments = Comment.latest(null, function() {
                for (var i = 0, l = scope.comments.length; i < l; i++) {
                    scope.comments[i].createdAt = moment(scope.comments[i].createdAt, 'X').format('MMMM D, YYYY [в] hh:mm');
                }
            });
        },
        templateUrl: 'js/directives/latestComments/latest-comments.html'
    };
}]);
