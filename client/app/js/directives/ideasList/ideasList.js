'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('ideasList', ['Idea', 'Tag', '$location', '$routeParams', function(Idea, Tag, $location, $routeParams) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            scope.filter = {tags: []};
            scope.isUserIdeas = attrs.userIdeas;

            var tags = ($location.search().tags || '').split(',');
            for (var j = 0, jj = tags.length; j < jj; j++) {
                if (tags[j]) {
                    scope.filter.tags.push({text: tags[j]});
                }
            }

            scope.isFilterShown = scope.filter.tags.length;

            scope.tabs = [
                {text: 'Новые', sort: 'new'},
                {text: 'Популярные', sort: 'popular'}
            ];

            var sort = $location.search().sort,
                curTab,
                limit = 20;

            if (sort) {
                for (var i = 0, l = scope.tabs.length; i < l; i++) {
                    if (scope.tabs[i].sort === sort) {
                        curTab = scope.tabs[i];
                        break;
                    }
                }
            }

            if (!curTab) {
                curTab = scope.tabs[0];
            }

            var page = $location.search().page;
            scope.hasPrev = false;
            scope.hasNext = false;

            curTab.active = true;

            function loadIdeas() {
                scope.loadInProgress = true;

                $location.search('page', page && page > 1 ? page: void 0);
                $location.search('sort', 'new' !== curTab.sort ? curTab.sort : void 0);

                var params = {sort: curTab.sort, page: page, limit: limit};
                if ($location.search().tags) {
                    params.tags = $location.search().tags;
                }

                if (scope.isUserIdeas && $routeParams.id) {
                    params.userId = $routeParams.id;
                }

                scope.ideas = Idea.query(params, function() {
                    scope.hasPrev = !!page && 1 < page;
                    scope.hasNext = scope.ideas.length >= limit;

                    scope.loadInProgress = false;
                });
            }

            loadIdeas();

            scope.activateTab = function(tab) {
                scope.hasPrev = false;
                scope.hasNext = false;

                if (curTab == tab) {
                    return;
                }

                curTab.active = false;
                curTab = tab;
                curTab.active = true;

                page = void 0;

                loadIdeas();
            };

            scope.loadMore = function(direction) {
                if ('next' === direction) {
                    page = page || 1;
                    page++;
                } else {
                    if (!page || page <= 1) {
                        return;
                    }

                    page--;
                }

                loadIdeas();
            };

            scope.loadTags = function(query) {
                return Tag.query({mask: query}).$promise;
            };

            scope.toggleFilter = function() {
                scope.isFilterShown = !scope.isFilterShown;
            };

            scope.tagsListChanged = function() {
                var tags = _.map(scope.filter.tags, function(t) { return t.text; });
                $location.search('tags', tags.length ? tags.join(',') : void 0);

                loadIdeas();
            };
        },
        templateUrl: 'js/directives/ideasList/ideas-list.html'
    };
}]);
