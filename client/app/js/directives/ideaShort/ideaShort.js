'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('ideaShort', ['auth', function(auth) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            idea: '='
        },
        link: function(scope, el, attrs) {
            scope.isListItem = attrs.isListItem;
            scope.isUltraShort = attrs.isUltraShort;
            scope.isUltraUltraShort = attrs.isUltraUltraShort;

            if (scope.idea.createdAt && parseInt(scope.idea.createdAt) == scope.idea.createdAt) {
                scope.idea.createdAt = moment(scope.idea.createdAt, 'X').format('MMMM D, YYYY');
            }

            scope.like = function() {
                if (auth.getUser()) {
                    if (scope.idea.user.id == auth.getUser().id) {
                        alert('Нельзя проголосовать за собственную идею (:');
                    } else if (scope.idea.isLiked) {
                        scope.idea.$noLongerLike(function() {
                            scope.idea.createdAt = moment(scope.idea.createdAt, 'X').format('MMMM D, YYYY');
                        });
                    } else {
                        scope.idea.$like(function() {
                            scope.idea.createdAt = moment(scope.idea.createdAt, 'X').format('MMMM D, YYYY');
                        });
                    }
                } else {
                    alert('Авторизуйтесь, пожалуйста (:')
                }
            };
        },
        templateUrl: 'js/directives/ideaShort/idea-short.html'
    };
}]);
