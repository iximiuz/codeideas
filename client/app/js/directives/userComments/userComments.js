'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('userComments', ['Comment', function(Comment) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            user: '='
        },
        link: function(scope, el, attrs) {
            scope.comments = [];
            scope.loaded = false;

            function _link() {
                scope.comments = Comment.getUserComments({userId: scope.user.id, limit: 20}, function() {
                    for (var i = 0, l = scope.comments.length; i < l; i++) {
                        scope.comments[i].createdAt = moment(scope.comments[i].createdAt, 'X').format('MMMM D, YYYY [в] hh:mm');
                    }

                    scope.loaded = true;
                });
            }

            if (scope.user.hasOwnProperty('$resolved') && !scope.user.$resolved) {
                scope.user.$promise.then(_link);
            } else {
                _link();
            }
        },
        templateUrl: 'js/directives/userComments/user-comments.html'
    };
}]);
