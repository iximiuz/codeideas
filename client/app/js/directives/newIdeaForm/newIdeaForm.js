'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('newIdeaForm', ['Idea', 'Tag', '$location', '$routeParams', 'auth', function(Idea, Tag, $location, $routeParams, auth) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            var idea = new Idea({});

            function _onSaveSuccess() {
                if (idea.user && idea.user.token && !auth.getUser()) {
                    auth.login(idea);
                }

                $location.path('/ideas/' + idea.id);
            }

            function _onSaveFailure() {
                scope.addition = false;
                scope.errorMsg = 'Извините, произошла ошибка. Попробуйте еще раз.';
            }

            scope.newIdea = {};
            scope.addition = false;
            scope.errorMsg = '';

            if ($routeParams.id) {
                idea = Idea.get({id: $routeParams.id}, function() {
                    if (idea.implementationTime) {
                        idea.devTime = {
                            1000: 'hours',
                            2000: 'days',
                            3000: 'weeks',
                            4000: 'months',
                            5000: 'geHalfOfYear'
                        }[idea.implementationTime];
                    }

                    idea.teamRequired = !!idea.teamType;

                    scope.newIdea = idea;
                });
            }

            scope.addIdea = function() {
                scope.addition = true;
                scope.errorMsg = '';

                for (var k in scope.newIdea) if (scope.newIdea.hasOwnProperty(k)) {
                    idea[k] = scope.newIdea[k];
                }

                if (idea.id) {
                    Idea.update({id: idea.id}, idea, _onSaveSuccess, _onSaveFailure);
                } else {
                    idea.$save(null, _onSaveSuccess, _onSaveFailure);
                }
            };

            scope.loadTags = function(query) {
                return Tag.query({mask: query}).$promise;
            };
        },
        templateUrl: 'js/directives/newIdeaForm/new-idea-form.html'
    };
}]);
