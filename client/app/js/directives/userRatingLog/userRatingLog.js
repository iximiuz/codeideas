'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('userRatingLog', ['RatingLog', function(RatingLog) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            user: '='
        },
        link: function(scope, el, attrs) {
            scope.ratingLog = [];
            scope.loaded = false;

            scope.pointsText = function(pointsAmount) {
                return pluralForm(pointsAmount, 'очко', 'очка', 'очков')
            };

            scope.eventText = function(r) {
                var result = '';

                switch (parseInt(r.event)) {
                    case 20000:
                        result += 'регистрация на сервисе CodeIdeas';
                        break;

                    case 20001:
                        result += 'идея пользователя была оценена другим участником';
                        break;

                    case 20002:
                        result += 'идея пользователя отмечена к реализации другим участником';
                        break;

                    case 20004:
                        result += 'использование тега пользователя другим участником';
                        break;

                    default:
                }

                // r.createdAt
                return result;
            };

            function _link() {
                scope.ratingLog = RatingLog.query({userId: scope.user.id}, function() {
                    scope.loaded = true;
                });
            }

            if (scope.user.hasOwnProperty('$resolved') && !scope.user.$resolved) {
                scope.user.$promise.then(_link);
            } else {
                _link();
            }
        },
        templateUrl: 'js/directives/userRatingLog/user-rating-log.html'
    };
}]);
