'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('latestIdeas', ['Idea', function(Idea) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            scope.ideas = Idea.query({limit: 5}, function() {
                for (var i = 0, l = scope.ideas.length; i < l; i++) {
                    scope.ideas[i].createdAt = moment(scope.ideas[i].createdAt, 'X').format('MMMM D, YYYY [в] hh:mm');
                }
            });
        },
        templateUrl: 'js/directives/latestIdeas/latest-ideas.html'
    };
}]);
