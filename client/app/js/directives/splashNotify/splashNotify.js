'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('splashNotify', ['auth', function(auth) {
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        link: function(scope, el, attrs) {
            scope.tmpUserHint = attrs.tmpUserHint;

            scope.closeMe = function() {
                localStorage.setItem('hideBindUsers:' + auth.getUser().id, 1);
                el.remove();
            };
        },
        templateUrl: 'js/directives/splashNotify/splash-notify.html'
    };
}]);
