'use strict';

var directives = angular.module('ciApp.directives');

directives.directive('tags', [function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            tags: '='
        },
        link: function(scope, el, attrs) {

        },
        templateUrl: 'js/directives/tags/tags.html'
    };
}]);
