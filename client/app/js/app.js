'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('ciApp', [
    'ngResource',
    'ngRoute',
    'ngTagsInput',

    'ciApp.controllers',
    'ciApp.directives',
    'ciApp.services'
]);

app.constant('CONFIG', {
    apiHost: 'http://' + window.location.host + '/api'
});

app.config(['$routeProvider', function($routeProvider) {
    // routing
    $routeProvider.when('/', {templateUrl: 'partials/ideas.html', controller: 'MainCtrl', reloadOnSearch: false});
    $routeProvider.when('/about', {templateUrl: 'partials/about.html', controller: 'MainCtrl'});
    $routeProvider.when('/ideas', {templateUrl: 'partials/ideas.html', controller: 'MainCtrl', reloadOnSearch: false});
    $routeProvider.when('/ideas/:id', {templateUrl: 'partials/idea.html', controller: 'MainCtrl'});
    $routeProvider.when('/ideas/:id/edit', {templateUrl: 'partials/edit-idea.html', controller: 'MainCtrl'});
    $routeProvider.when('/new-idea', {templateUrl: 'partials/new-idea.html', controller: 'MainCtrl'});
    $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'MainCtrl'});
    $routeProvider.when('/login/classic', {templateUrl: 'partials/login.html', controller: 'MainCtrl'});
    $routeProvider.when('/register', {templateUrl: 'partials/register.html', controller: 'MainCtrl'});
    $routeProvider.when('/users', {templateUrl: 'partials/users.html', controller: 'MainCtrl', reloadOnSearch: false});
    $routeProvider.when('/users/:id', {templateUrl: 'partials/user.html', controller: 'MainCtrl'});
    $routeProvider.when('/users/:id/ideas', {templateUrl: 'partials/user-ideas.html', controller: 'MainCtrl'});
    $routeProvider.otherwise({redirectTo: '/ideas'});
}]);

app.run(['$rootScope', '$location', '$anchorScroll', '$routeParams', 'auth', function($rootScope, $location, $anchorScroll, $routeParams, auth) {
    //when the route is changed scroll to the proper element.
    $rootScope.$on('$routeChangeSuccess', function(next, current) {
        $location.hash($routeParams.scrollTo);
        $anchorScroll();
        $location.search('scrollTo', null);
    });

    auth.tryLogin(function() {
        if (auth.getUser() && '/enter' === $location.path()) {
            $location.path('/ideas');
        }
    });

    // register listener to watch route changes
    $rootScope.$on('$routeChangeStart', function(event, next, current) {
        if (auth.getUser() && 'partials/enter.html' === next.templateUrl) {
            $location.path(current && '/enter' !== current.originalPath ? current.originalPath : '/ideas');
        }
    });
}]);