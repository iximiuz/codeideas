'use strict';

/* Controllers */

var controllers = angular.module('ciApp.controllers', []);
controllers.controller('MainCtrl', ['$scope', '$location', '$routeParams', '$rootScope', '$window', 'auth', 'User', function($scope, $location, $routeParams, $rootScope, $window, auth, User) {
    $scope.$on('$viewContentLoaded', function(event) {
        $window.ga('send', 'pageview', {page: $location.path()});


        $scope.showSplashNotify = {
            bindUsers: (!!auth.getUser() && !!auth.getUser().isTmp && !localStorage.getItem('hideBindUsers:' + auth.getUser().id))
        };
    });

    $scope.canAddNewIdea = function() {
        return '/new-idea' !== $location.path();
    };

    $scope.needHighlightMenuItem = function(itemRoute) {
        return $location.path() === ('/' + itemRoute) ? 'active' : '';
    };

    $scope.getUser = function() {
        return auth.getUser();
    };

    $scope.logout = function() {
        auth.logout();
    };

    $scope.hideSplash = {
        welcome: localStorage.getItem('hideSplash:welcome')
    };

    $scope.closeSplash = function(id) {
        localStorage.setItem('hideSplash:' + id, 1);
        $scope.hideSplash[id] = true;
    };

    if ($location.path().match(/^\/users\/\d+\/ideas/)) {
        $scope.user = auth.getUserId() == $routeParams.id ? auth.getUser() : User.get({id: $routeParams.id});
    }
}]);